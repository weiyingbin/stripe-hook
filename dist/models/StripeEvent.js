"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_paginate_1 = __importDefault(require("mongoose-paginate"));
const stripeEventSchema = new mongoose_1.default.Schema({
    id: String,
    stripeEvtId: String,
    orderID: String,
    username: String,
    event: String,
    comment: String,
    data: { type: mongoose_1.default.Schema.Types.Mixed }
}, { timestamps: true });
stripeEventSchema.plugin(mongoose_paginate_1.default);
stripeEventSchema.post('save', (doc, next) => __awaiter(this, void 0, void 0, function* () {
    yield doc.update({ id: doc._id.toString() });
    next();
}));
exports.StripeEventModel = mongoose_1.default.model('StripeEvent', stripeEventSchema);
exports.default = exports.StripeEventModel;
//# sourceMappingURL=StripeEvent.js.map