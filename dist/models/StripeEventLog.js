"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_paginate_1 = __importDefault(require("mongoose-paginate"));
const stripeEventLogSchema = new mongoose_1.default.Schema({
    id: String,
    stripeEvtId: String,
    orderID: String,
    student: String,
    event: String,
    eventType: String,
    data: { type: mongoose_1.default.Schema.Types.Mixed },
}, { timestamps: true });
stripeEventLogSchema.plugin(mongoose_paginate_1.default);
stripeEventLogSchema.post('save', (doc, next) => __awaiter(this, void 0, void 0, function* () {
    yield doc.update({ id: doc._id.toString() });
    next();
}));
exports.StripeEventLogModel = mongoose_1.default.model('StripeEventLog', stripeEventLogSchema);
exports.default = exports.StripeEventLogModel;
//# sourceMappingURL=StripeEventLog.js.map