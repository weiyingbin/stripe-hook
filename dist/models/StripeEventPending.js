"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_paginate_1 = __importDefault(require("mongoose-paginate"));
const StripeEventLog_1 = __importDefault(require("./StripeEventLog"));
const stripeEventPendingSchema = new mongoose_1.default.Schema({
    id: String,
    eventLog: { type: mongoose_1.default.Schema.Types.ObjectId, ref: 'StripeEventLog' },
    orderID: String,
    event: String,
    eventType: String,
    reminded: { type: mongoose_1.default.Schema.Types.Boolean, default: false },
}, { timestamps: true });
stripeEventPendingSchema.plugin(mongoose_paginate_1.default);
stripeEventPendingSchema.post('save', (doc, next) => __awaiter(this, void 0, void 0, function* () {
    yield doc.update({ id: doc._id.toString() });
    next();
}));
exports.StripeEventPendingModel = mongoose_1.default.model('StripeEventPending', stripeEventPendingSchema);
exports.default = StripeEventLog_1.default;
//# sourceMappingURL=StripeEventPending.js.map