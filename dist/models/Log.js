"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_paginate_1 = __importDefault(require("mongoose-paginate"));
const logSchema = new mongoose_1.default.Schema({
    id: String,
    board: String,
    section: String,
    user: { type: mongoose_1.default.Schema.Types.ObjectId, ref: 'User' },
    action: String,
    type: String,
    comment: String,
    data: String
}, { timestamps: true });
logSchema.plugin(mongoose_paginate_1.default);
logSchema.post('save', (doc, next) => __awaiter(this, void 0, void 0, function* () {
    yield doc.update({ id: doc._id.toString() });
    next();
}));
exports.LoggerModel = mongoose_1.default.model('Log', logSchema);
exports.default = exports.LoggerModel;
//# sourceMappingURL=Log.js.map