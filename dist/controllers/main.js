"use strict";
/**
 * Hook main function
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const stripe_1 = __importDefault(require("stripe"));
const secrets_1 = require("./../util/secrets");
const StripeEventLog_1 = __importDefault(require("../models/StripeEventLog"));
const StripeEventPending_1 = require("./../models/StripeEventPending");
function main(req, res) {
    // Validate events are sent by Stripe, not a third party
    let sig = req.headers['stripe-signature'];
    try {
        let event = new stripe_1.default(secrets_1.STRIPE_PK).webhooks.constructEvent(req.body, sig, secrets_1.STRIPE_ENDPOINT_SECRET);
        // For testing purposes, log the req body to the console
        // to compare with the req body shown in the Dashboard.
        // In a production environment, you'll want to store this
        // event in your database.
        console.log('WEBHOOK API DATA:\n\n\n');
        console.log(event);
        console.log({ metadata: event.data.object.metadata });
        console.log('WEBHOOK END\n\n\n');
        // Do something with event
        let metadata;
        try {
            metadata = event.data.object.metadata;
        }
        catch (error) {
            console.log({ error });
        }
        logEvent(event, metadata);
    }
    catch (err) {
        console.log({ err });
        return res.status(400);
    }
    // Return a res to acknowledge receipt of the event
    return res.json({ received: true });
}
exports.default = main;
function logEvent(event, metadata) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!metadata.orderID && !metadata.student)
            metadata.orderID = 'test';
        const newLog = yield createDBEventLog(event, metadata);
        if (newLog && newLog.eventType == 'source.chargeable')
            createPendingEvent(newLog);
        if (newLog && newLog.eventType == 'charge.succeeded')
            removePendingEvent(newLog);
    });
}
function createDBEventLog(event, metadata) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const _newLog = {
                orderID: metadata.orderID,
                student: metadata.student,
                event: event.data.object.object,
                eventType: event.type,
                stripeEvtId: event.id,
                data: event,
            };
            const newLog = (yield new StripeEventLog_1.default(_newLog).save());
            console.log({ newLog });
            return newLog;
        }
        catch (error) {
            console.error('Unable to create event log', metadata.orderID);
            return undefined;
        }
    });
}
function createPendingEvent(nl) {
    return __awaiter(this, void 0, void 0, function* () {
        const _newPending = {
            event: nl.event,
            eventLog: nl._id,
            eventType: nl.eventType,
            orderID: nl.orderID,
        };
        try {
            const newPending = yield new StripeEventPending_1.StripeEventPendingModel(_newPending).save();
            console.log({ newPending });
        }
        catch (error) {
            console.error('Unable to create pending event ', nl.orderID);
            console.error(error);
        }
    });
}
function removePendingEvent(nl) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const removed = yield StripeEventPending_1.StripeEventPendingModel.remove({ orderID: nl.orderID });
            console.log({ removed });
        }
        catch (error) {
            console.error('Unable to remove pending event ', nl.orderID);
            console.error(error);
        }
    });
}
//# sourceMappingURL=main.js.map