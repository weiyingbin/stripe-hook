"use strict";
/**
 * System logs and traces of users & system activities
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// store to db
const Log_1 = require("../models/Log");
// store to file
const logger_1 = require("../util/logger");
var LogType;
(function (LogType) {
    LogType["ERROR"] = "ERROR";
    LogType["INFO"] = "INFO";
    LogType["WARN"] = "WARN";
    LogType["DEBUG"] = "DEBUG";
})(LogType = exports.LogType || (exports.LogType = {}));
var LogCategory;
(function (LogCategory) {
    LogCategory["EMAIL"] = "EMAIL";
    LogCategory["AUTH"] = "AUTH";
    LogCategory["FILEUPLOAD"] = "FILEUPLOAD";
    LogCategory["DATABASE"] = "DATABASE";
    LogCategory["WEBSOCKET"] = "WEBSOCKET";
})(LogCategory = exports.LogCategory || (exports.LogCategory = {}));
/**
 * Kinda like the bigbrother that knows everything
 * Keeping logs of all the activities to ensure system safety
 * @param leIncomingObject an object describing the situation in which a log is required
 */
exports.Logger = (leIncomingObject) => __awaiter(this, void 0, void 0, function* () {
    // Log to DB
    if (typeof leIncomingObject.data != 'string') {
        // tslint:disable-next-line:no-null-keyword
        leIncomingObject.data = JSON.stringify(leIncomingObject.data, null, '\t');
    }
    const log = new Log_1.LoggerModel(leIncomingObject);
    try {
        yield log.save();
    }
    catch (e) {
        // this is worst case scenario so we gotta log this into a file at least
        console.log(e, leIncomingObject);
        logger_1.logger.error(JSON.stringify(leIncomingObject));
        logger_1.logger.error(e);
    }
    // Log to file / console
    if (leIncomingObject.type === 'ERROR') {
        logger_1.logger.error(JSON.stringify(leIncomingObject));
    }
    else {
        logger_1.logger.debug(JSON.stringify(leIncomingObject));
    }
});
exports.default = exports.Logger;
//# sourceMappingURL=logger.js.map