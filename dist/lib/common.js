"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Constant_1 = __importDefault(require("../models/Constant"));
const Feedback_1 = __importDefault(require("../models/Feedback"));
const functions_1 = require("./functions");
const logger_1 = require("../controllers/logger");
const User_1 = __importDefault(require("../models/User"));
const Option_1 = __importDefault(require("../models/Option"));
exports.getConstByKey = (key) => __awaiter(this, void 0, void 0, function* () {
    return (yield Constant_1.default.findOne({ key }));
});
exports.getOptionByKey = (key) => __awaiter(this, void 0, void 0, function* () {
    return (yield Option_1.default.findOne({ key }));
});
exports.getConstantPhraseById = (_id) => __awaiter(this, void 0, void 0, function* () {
    const phrase = (yield Constant_1.default.findOne({ _id }, 'phrase'));
    if (!phrase) {
        return Promise.resolve(undefined);
    }
    return Promise.resolve(phrase.phrase);
});
exports.getConstById = (id) => __awaiter(this, void 0, void 0, function* () {
    return yield Constant_1.default.findById(id, 'key value phrase description extra');
});
/**
 * @api {post} /feedback Send Feedback
 * @apiName Send Feedback
 * @apiPermission Users
 * @apiGroup common
 *
 * @apiParam {String} [feedback] feedback
 *
 * @apiSuccess (200) {Object} mixed `success`
 *
 */
exports.sendFeedback = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    req.assert('feedback', 'feedback is required').notEmpty();
    if (req.validationErrors()) {
        return res.send();
    }
    const alerts = [];
    const feedback = new Feedback_1.default({
        content: req.body.feedback,
    });
    if (req.body.anonymous === true) {
        feedback.set('anonymous', true);
    }
    else {
        feedback.set('user', req.user._id);
    }
    try {
        yield feedback.save();
    }
    catch (e) {
        logger_1.Logger({
            type: logger_1.LogType.ERROR,
            action: 'sendFeedback',
            data: { stack: e, feedback },
            comment: 'Failed to save feedback',
            board: 'student',
            section: 'order.ts calculating total',
        });
        exports.unexpectedErrorAlert(alerts);
        return res.send({ alerts });
    }
    // sendEmail.newFeedback(feedback.content, req.body.anonymous ? undefined : req.user.username);
    return res.send({
        success: true,
    });
});
exports.invalidInputAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'invalidInput',
        type: 'error',
        timeout: 10000,
    });
};
exports.unauthorizedOPErrorAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'unauthorizedOP',
        type: 'error',
        timeout: 10000,
    });
};
exports.orderNotFoundAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'orderNotFound',
        type: 'error',
        timeout: 10000,
    });
};
exports.orderIDReqErrorAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'orderIDRequired',
        type: 'error',
        timeout: 10000,
    });
};
exports.orderUpdatedSuccessfullyAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'orderUpdatedSuccessfully',
        type: 'success',
        timeout: 10000,
    });
};
exports.formUpdatedSuccessfullyAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'formUpdatedSuccessfully',
        type: 'success',
        timeout: 10000,
    });
};
exports.operationCompletedSuccessfullyAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'operationCompletedSuccessfully',
        type: 'success',
        timeout: 10000,
    });
};
exports.unexpectedErrorAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'unexpectedErrorHappened',
        type: 'error',
        timeout: 10000,
    });
};
exports.userDoesNotExistAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'userDoesNotExist',
        type: 'error',
        timeout: 10000,
    });
};
exports.internalErrorAlert = (alerts) => {
    functions_1.makeAlert(alerts, {
        content: 'internalError',
        type: 'error',
        timeout: 10000,
    });
};
exports.getUserById = (userID, requestingUser = userID) => __awaiter(this, void 0, void 0, function* () {
    let user;
    try {
        user = (yield User_1.default.findById(userID));
        return user;
    }
    catch (error) {
        logger_1.Logger({
            type: logger_1.LogType.ERROR,
            action: 'getUserById',
            comment: 'failed to fetch user by id',
            data: { userID, stack: error.toString() },
            section: 'order',
            board: 'admin',
            user: requestingUser,
        });
        return undefined;
    }
});
//# sourceMappingURL=common.js.map