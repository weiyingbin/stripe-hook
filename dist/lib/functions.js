"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Option_1 = __importDefault(require("../models/Option"));
const logger_1 = require("../controllers/logger");
const User_1 = __importDefault(require("../models/User"));
exports.getOption = (option, valueOnly = true) => __awaiter(this, void 0, void 0, function* () {
    if (valueOnly) {
        const op = yield Option_1.default.findOne({ key: option }, 'value');
        if (!op) {
            return undefined;
        }
        return op.value;
    }
    else {
        const op = yield Option_1.default.findOne({ key: option });
        if (!op) {
            return undefined;
        }
        return op;
    }
});
exports.makeAlert = (alerts, { timeout, type, content }) => {
    alerts.push({ type, content, timeout });
};
exports.getClientUrl = () => __awaiter(this, void 0, void 0, function* () {
    const mode = yield exports.getOption('mode');
    if (mode === 'development') {
        return yield exports.getOption('client_url_dev');
    }
    return yield exports.getOption('client_url');
});
exports.getServerUrl = () => __awaiter(this, void 0, void 0, function* () {
    const mode = yield exports.getOption('mode');
    if (mode === 'development') {
        return yield exports.getOption('server_url_dev');
    }
    return yield exports.getOption('server_url');
});
exports.getTopAdmin = () => __awaiter(this, void 0, void 0, function* () {
    const topAdminID = yield exports.getOption('topAdmin');
    if (!topAdminID)
        return;
    try {
        return (yield User_1.default.findById(topAdminID).select('-password'));
    }
    catch (error) {
        logger_1.Logger({
            type: logger_1.LogType.ERROR,
            action: 'getTopAdmin',
            data: {
                stack: error.toString(),
            },
            comment: 'Failed to get topAdmin',
            board: 'functions',
            section: 'functions.ts',
        });
        return;
    }
});
/**
 * Get a user's email
 * @param {string} userID user id
 */
exports.getUserEmail = (userID) => __awaiter(this, void 0, void 0, function* () {
    const user = yield exports.getUserByID(userID);
    if (!user)
        return;
    return user.email;
});
/**
 * Get a user
 * @param {string} userID user id
 * @return {UserModel} user
 */
exports.getUserByID = (userID) => __awaiter(this, void 0, void 0, function* () {
    try {
        const user = (yield User_1.default.findById(userID));
        if (!user)
            return;
        return user;
    }
    catch (error) {
        logger_1.Logger({
            type: logger_1.LogType.ERROR,
            action: 'getUserEmail',
            data: {
                stack: error.toString(),
            },
            comment: 'Failed to get user profile & email',
            board: 'functions',
            section: 'functions.ts',
        });
        return;
    }
});
/**
 * this function serves as a helper to determine
 * if a certain uploaded file is an image by its mimetype info
 * @param file file object uploaded by the express middleware coming from the client
 */
exports.isMimeImage = (file) => {
    return file.mimetype.split('/').indexOf('image') >= 0;
};
/**
 * Cleans string from special chars
 * then adds a wild card between spaces for improved filter results
 * @param s The string to use for filter
 */
exports.cleanDBRegExW = (s) => {
    try {
        s = exports.cleanDBRegEx(s);
        s = s.replace(/ /g, '(.*)');
        return s;
    }
    catch (error) {
        logger_1.Logger({
            type: logger_1.LogType.ERROR,
            action: 'cleanDBRegExW',
            data: {
                stack: error.toString(),
            },
            comment: 'Failed to user regex for expressions',
            board: 'admin',
            section: 'admin.ts',
        });
        return '';
    }
};
/**
 * Cleans string from special chars
 * @param s The string to use for filter
 */
exports.cleanDBRegEx = (s) => {
    try {
        s = s.replace(/[^a-zA-Z0-9 ]/g, ' ');
        s = s.trim();
        s = s.replace(/ {2,}/g, ' ');
        return s;
    }
    catch (error) {
        logger_1.Logger({
            type: logger_1.LogType.ERROR,
            action: 'cleanDBRegEx',
            data: {
                stack: error.toString(),
            },
            comment: 'Failed to user regex for expressions',
            board: 'admin',
            section: 'admin.ts',
        });
        return '';
    }
};
exports.updateUserLastSeen = (userID) => __awaiter(this, void 0, void 0, function* () {
    try {
        const som = yield User_1.default.updateOne({ _id: userID }, { lastSeen: new Date() });
        // console.log('here updated', userID, som, new Date());
    }
    catch (error) {
        console.log('failed to update DB last seen', error.toString());
    }
});
//# sourceMappingURL=functions.js.map