"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const compression_1 = __importDefault(require("compression")); // compresses requests
const body_parser_1 = __importDefault(require("body-parser"));
const lusca_1 = __importDefault(require("lusca"));
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
const passport_1 = __importDefault(require("passport"));
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const bluebird_1 = __importDefault(require("bluebird"));
const mongoose_1 = __importDefault(require("mongoose"));
const secrets_1 = require("./util/secrets");
const main_1 = __importDefault(require("./controllers/main"));
// Load environment variables from .env file, where API keys and passwords are configured
dotenv_1.default.config({ path: '.env' });
// Controllers (route handlers)
// Create Express server
const app = express_1.default();
// Connect to MongoDB
const mongoUrl = secrets_1.MONGODB_URI;
mongoose_1.default.Promise = bluebird_1.default;
mongoose_1.default
    .connect(mongoUrl, {
    connectTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30,
})
    .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
})
    .catch((err) => {
    console.log('MongoDB connection error. Please make sure MongoDB is running. ' + err);
    // process.exit();
});
// Express configuration
app.set('port', process.env.PORT || 3003);
app.use(compression_1.default());
app.use(body_parser_1.default.raw({ type: '*/*' }));
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(passport_1.default.initialize());
app.use(lusca_1.default.xframe('SAMEORIGIN'));
app.use(lusca_1.default.p3p('ABCDEF'));
app.use(lusca_1.default.hsts({ maxAge: 31536000, includeSubDomains: true, preload: true }));
app.use(lusca_1.default.xssProtection(true));
app.use(lusca_1.default.nosniff());
app.use(lusca_1.default.referrerPolicy('same-origin'));
app.use(express_1.default.static(path_1.default.join(__dirname, 'public'), { maxAge: 31557600000 }));
app.enable('trust proxy');
app.use(new express_rate_limit_1.default({
    windowMs: 5 * 60 * 1000,
    max: 1000,
}));
const allowCrossDomain = function (req, res, next) {
    console.log('got request', req.body, req.hostname);
    const allowedOrigins = ['*'];
    const origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.header('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
};
app.use(allowCrossDomain);
app.disable('x-powered-by');
app.post('/hook', main_1.default);
exports.default = app;
//# sourceMappingURL=app.js.map