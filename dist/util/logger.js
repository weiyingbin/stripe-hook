"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = __importDefault(require("winston"));
const loggerOptions = {
    transports: [
        new winston_1.default.transports.Console({
            level: 'debug',
        }),
    ],
};
console.log('env mode', process.env.MODE);
/*if (process.env.MODE != 'test') {
  loggerOptions.transports.push(
    new winston.transports.File({
      filename: `logs/debug-${new Date().toISOString().split('T')[0]}.log`,
      level: 'debug',
    })
  );
} else {
  loggerOptions.transports.push(
    new winston.transports.File({
      filename: `../tmp/logs/debug-${
        new Date().toISOString().split('T')[0]
      }.log`,
      level: 'debug',
    })
  );
}*/
exports.logger = winston_1.default.createLogger(loggerOptions);
exports.logger.debug('Logging initialized at debug level');
exports.default = exports.logger;
//# sourceMappingURL=logger.js.map