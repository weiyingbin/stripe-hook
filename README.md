# EZ Stripe Hook

Work in progress

The main purpose of this repository is to show a good end-to-end project setup and workflow for writing Node code in TypeScript.



# Pre-reqs
To build and run this app locally you will need a few things:
- Install [Node.js](https://nodejs.org/en/)
- Install [MongoDB](https://docs.mongodb.com/manual/installation/)
- Install [VS Code](https://code.visualstudio.com/)