/**
 * Cronjobs or timed tasks init
 */
import * as nodeCron from 'node-cron';

/**
 * Cron functions
 */
const cron = () => {
  console.log('Cron started');
};
export default cron;
