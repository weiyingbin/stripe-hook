import express from 'express';
import compression from 'compression'; // compresses requests
import bodyParser from 'body-parser';
import lusca from 'lusca';
import dotenv from 'dotenv';
import path from 'path';
import RateLimit from 'express-rate-limit';
import bluebird from 'bluebird';
import mongoose from 'mongoose';
import { MONGODB_URI } from './util/secrets';
import main from './controllers/main';


// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env' });

// Controllers (route handlers)

// Create Express server
const app = express();

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
(<any>mongoose).Promise = bluebird;
mongoose
  .connect(
    mongoUrl,
    {
      connectTimeoutMS: 30000,
      keepAlive: true,
      reconnectTries: 30,
    }
  )
  .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
  })
  .catch((err: Error) => {
    console.log('MongoDB connection error. Please make sure MongoDB is running. ' + err);
    // process.exit();
  });

// Express configuration
app.set('port', process.env.PORT || 3003);
app.use(compression());
app.use(bodyParser.raw({type: '*/*'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.p3p('ABCDEF'));
app.use(lusca.hsts({ maxAge: 31536000, includeSubDomains: true, preload: true }));
app.use(lusca.xssProtection(true));
app.use(lusca.nosniff());
app.use(lusca.referrerPolicy('same-origin'));


app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));
app.enable('trust proxy');
app.use(
  new RateLimit({
    windowMs: 5 * 60 * 1000, // 5 minutes
    max: 1000, // limit each IP to 100 requests per windowMs
  })
);

const allowCrossDomain = function(req: any, res: any, next: any) {
  console.log('got request', req.body, req.hostname);
  const allowedOrigins = ['*'];
  const origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.header('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  next();
};

app.use(allowCrossDomain);
app.disable('x-powered-by');


app.post('/hook', main);

export default app;
