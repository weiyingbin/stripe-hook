import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

export interface IStripeEventLog {
  id?: string;
  stripeEvtId: string;
  orderID: string;
  student: string;
  event: string;
  eventType: string;
  data: any;
}
export type stripeEventLogType = mongoose.Document & {
  id?: string;
  stripeEvtId: string;
  orderID: string;
  student: string;
  event: string;
  eventType: string;
  data: any;
};

const stripeEventLogSchema = new mongoose.Schema(
  {
    id: String,
    stripeEvtId: String,
    orderID: String,
    student: String,
    event: String,
    eventType: String,
    data: { type: mongoose.Schema.Types.Mixed },
  },
  { timestamps: true }
);
stripeEventLogSchema.plugin(mongoosePaginate);

stripeEventLogSchema.post('save', async (doc: mongoose.Document, next: Function) => {
  await doc.update({ id: doc._id.toString() });
  next();
});

export const StripeEventLogModel = mongoose.model('StripeEventLog', stripeEventLogSchema);
export default StripeEventLogModel;
