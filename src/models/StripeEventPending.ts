import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import StripeEventLogModel, { stripeEventLogType } from './StripeEventLog';

export interface IStripeEventPending {
  id?: string;
  eventLog: stripeEventLogType;
  orderID: string;
  event: string;
  eventType: string;
  reminded?: boolean;
}

export type StripeEventPendingType = mongoose.Document & {
  id: string;
  eventLog: stripeEventLogType;
  orderID: string;
  event: string;
  eventType: string;
  reminded: boolean;
};

const stripeEventPendingSchema = new mongoose.Schema(
  {
    id: String,
    eventLog: { type: mongoose.Schema.Types.ObjectId, ref: 'StripeEventLog' },
    orderID: String,
    event: String,
    eventType: String,
    reminded: { type: mongoose.Schema.Types.Boolean, default: false },
  },
  { timestamps: true }
);
stripeEventPendingSchema.plugin(mongoosePaginate);

stripeEventPendingSchema.post('save', async (doc: mongoose.Document, next: Function) => {
  await doc.update({ id: doc._id.toString() });
  next();
});

export const StripeEventPendingModel = mongoose.model('StripeEventPending', stripeEventPendingSchema);
export default StripeEventLogModel;
