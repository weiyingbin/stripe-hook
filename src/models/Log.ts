import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const logSchema = new mongoose.Schema(
  {
    id: String,
    board: String,
    section: String,
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    action: String,
    type: String,
    comment: String,
    data: String
  },
  { timestamps: true }
);
logSchema.plugin(mongoosePaginate);

logSchema.post('save', async (doc: mongoose.Document, next: Function) => {
  await doc.update({id: doc._id.toString()});
  next();
});

export const LoggerModel = mongoose.model('Log', logSchema);
export default LoggerModel;
