import winston from 'winston';

const loggerOptions: any = {
  transports: [
    new winston.transports.Console({
      level: 'debug',
    }),
  ],
};
console.log('env mode', process.env.MODE);
/*if (process.env.MODE != 'test') {
  loggerOptions.transports.push(
    new winston.transports.File({
      filename: `logs/debug-${new Date().toISOString().split('T')[0]}.log`,
      level: 'debug',
    })
  );
} else {
  loggerOptions.transports.push(
    new winston.transports.File({
      filename: `../tmp/logs/debug-${
        new Date().toISOString().split('T')[0]
      }.log`,
      level: 'debug',
    })
  );
}*/

export const logger = winston.createLogger(loggerOptions);
logger.debug('Logging initialized at debug level');

export default logger;
