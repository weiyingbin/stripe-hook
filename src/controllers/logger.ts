/**
 * System logs and traces of users & system activities
 */

// store to db
import { LoggerModel as _Logger } from '../models/Log';
// store to file
import { logger as WinstonLogger } from '../util/logger';

export enum LogType {
  ERROR = 'ERROR',
  INFO = 'INFO',
  WARN = 'WARN',
  DEBUG = 'DEBUG',
}
export enum LogCategory {
  EMAIL = 'EMAIL',
  AUTH = 'AUTH',
  FILEUPLOAD = 'FILEUPLOAD',
  DATABASE = 'DATABASE',
  WEBSOCKET = 'WEBSOCKET',
}

export interface LogModel {
  cat?: LogCategory;
  board?: string;
  section?: string;
  user?: string;
  action?: string;
  type?: LogType;
  comment?: string;
  data?: any;
}

/**
 * Kinda like the bigbrother that knows everything
 * Keeping logs of all the activities to ensure system safety
 * @param leIncomingObject an object describing the situation in which a log is required
 */
export const Logger = async (leIncomingObject: LogModel) => {
  // Log to DB
  if (typeof leIncomingObject.data != 'string') {
    // tslint:disable-next-line:no-null-keyword
    leIncomingObject.data = JSON.stringify(leIncomingObject.data, null, '\t');
  }

  const log = new _Logger(leIncomingObject);
  try {
    await log.save();
  } catch (e) {
    // this is worst case scenario so we gotta log this into a file at least
    console.log(e, leIncomingObject);
    WinstonLogger.error(JSON.stringify(leIncomingObject));
    WinstonLogger.error(e);
  }

  // Log to file / console
  if (leIncomingObject.type === 'ERROR') {
    WinstonLogger.error(JSON.stringify(leIncomingObject));
  } else {
    WinstonLogger.debug(JSON.stringify(leIncomingObject));
  }
};

export default Logger;
