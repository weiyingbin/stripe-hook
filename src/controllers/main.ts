/**
 * Hook main function
 */

import { Request, Response } from 'express';
import { default as StripeSDK } from 'stripe';
import { STRIPE_PK, STRIPE_ENDPOINT_SECRET } from './../util/secrets';
import StripeEventLogModel, { IStripeEventLog, stripeEventLogType } from '../models/StripeEventLog';
import { IStripeEventPending, StripeEventPendingModel } from './../models/StripeEventPending';
import { IMetadata } from './interfaces';

export default function main(req: Request, res: Response) {
  // Validate events are sent by Stripe, not a third party
  let sig = req.headers['stripe-signature'];

  try {
    let event = new StripeSDK(STRIPE_PK).webhooks.constructEvent(req.body, sig, STRIPE_ENDPOINT_SECRET);

    // For testing purposes, log the req body to the console
    // to compare with the req body shown in the Dashboard.
    // In a production environment, you'll want to store this
    // event in your database.
    console.log('WEBHOOK API DATA:\n\n\n');
    console.log(event);
    console.log({ metadata: (event.data.object as any).metadata });
    console.log('WEBHOOK END\n\n\n');

    // Do something with event
    let metadata;
    try {
      metadata = (event.data.object as any).metadata as IMetadata;
    } catch (error) {
      console.log({ error });
    }
    logEvent(event, metadata);
  } catch (err) {
    console.log({ err });
    return res.status(400);
  }

  // Return a res to acknowledge receipt of the event
  return res.json({ received: true });
}

async function logEvent(event: StripeSDK.events.IEvent, metadata: IMetadata) {
  if (!metadata.orderID && !metadata.student) metadata.orderID = 'test';
  const newLog = await createDBEventLog(event, metadata);
  if (newLog && newLog.eventType == 'source.chargeable') createPendingEvent(newLog);
  if (newLog && newLog.eventType == 'charge.succeeded') removePendingEvent(newLog);
}

async function createDBEventLog(event: StripeSDK.events.IEvent, metadata: IMetadata): Promise<stripeEventLogType> | undefined {
  try {
    const _newLog: IStripeEventLog = {
      orderID: metadata.orderID,
      student: metadata.student,
      event: event.data.object.object,
      eventType: event.type,
      stripeEvtId: event.id,
      data: event,
    };
    const newLog = (await new StripeEventLogModel(_newLog).save()) as stripeEventLogType;
    console.log({ newLog });
    return newLog;
  } catch (error) {
    console.error('Unable to create event log', metadata.orderID);
    return undefined;
  }
}

async function createPendingEvent(nl: stripeEventLogType) {
  const _newPending: IStripeEventPending = {
    event: nl.event,
    eventLog: nl._id,
    eventType: nl.eventType,
    orderID: nl.orderID,
  };
  try {
    const newPending = await new StripeEventPendingModel(_newPending).save();
    console.log({ newPending });
  } catch (error) {
    console.error('Unable to create pending event ', nl.orderID);
    console.error(error);
  }
}

async function removePendingEvent(nl: stripeEventLogType) {
  try {
    const removed = await StripeEventPendingModel.remove({ orderID: nl.orderID });
    console.log({ removed });
  } catch (error) {
    console.error('Unable to remove pending event ', nl.orderID);
    console.error(error);
  }
}
